[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=alert_status)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=coverage)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=sqale_index)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=code_smells)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=bugs)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=Thegaijin_recipesapi&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=Thegaijin_recipesapi)

# RecipeAPI

This is an API for a Recipes service using Flask. Users can register an account and login to create, edit, view and delete recipe categories and recipes in those categories.

Add link to Heroku here

| EndPoint                                          | Functionality                                    |
| ------------------------------------------------- | ------------------------------------------------ |
| [ POST /auth/login/ ](#)                          | Logs a user in                                   |
| [ POST /auth/register/ ](#)                       | Register a user                                  |
| [ DELETE /auth/logout/ ](#)                       | Logout a user                                    |
| [ POST /categories/ ](#)                          | Create a new category                            |
| [ GET /categories/ ](#)                           | Get all categories created by the logged in user |
| [ GET /categories/\<category_id>/ ](#)            | Get a category by it's id                        |
| [ PUT /categories/\<category_id>/ ](#)            | Update the category                              |
| [ DELETE /categories/\<category_id>/ ](#)         | Delete the category                              |
| [ POST /recipes/\<category_id>/ ](#)              | Create a recipe in the specified category        |
| [ GET /recipes/](#)                               | Get all recipes created by the logged in user    |
| [ GET /recipes/\<category_id>/](#)                | Get all recipes in the specified category id     |
| [ GET /recipes/\<category_id>/\<recipe_id>](#)    | Get a recipe in the specified category id        |
| [ PUT /recipes/\<category_id>/<recipe_id> ](#)    | Update the recipe in the specified category id   |
| [ DELETE /recipes/\<category_id>/<recipe_id> ](#) | Delete the recipe in the specified category id   |

## Setup

To use the application, ensure that you have python 3.6+, clone the repository to your local machine. Open your git commandline and run

1. Clone the repository

   ```
   git clone https://github.com/Thegaijin/recipeAPI.git
   ```

2. Enter the project directory
   ```
   cd recipeAPI
   ```
3. Create a virtual environment
   ```
   virtualenv venv
   ```
4. Activate the virtual environment
   ```
   source venv/bin/activate
   ```
5. Then install all the required dependencies:
   ```
   pip install -r requirements.txt
   ```
6. Install postgres if you don't already have it. Preferably Postgres 10.1.

7. Create the Databases

   #### For the test Database

   ```
   createdb test_db
   ```

   #### For the development Database

   ```
   createdb recipe_db
   ```

8. Run Migrations using these commands, in that order:

   ```
   python manage.py db init
   python manage.py db migrate
   python manage.py db upgrade
   ```

9. To test the application, run the command:

   ```
   pytest --cov-report term --cov=app
   ```

10. To start the server, run the command:

```
export FLASK_CONFIG=development
python manage.py runserver
```
